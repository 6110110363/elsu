import guess.guess_number as guess_number
import unittest
from unittest import mock

class guessNumberTest(unittest.TestCase):
    #guess_int
    @mock.patch('random.randint',return_value=int(0))
    def test_guess_int_0_to_0_should_be_0(self, guessValue):
        result = guess_number.guess_int(0,0)
        self.assertEqual(result,0,"should be 0")
    
    @mock.patch('random.randint',return_value=int(7))
    def test_guess_int_1_to_10_should_be_7(self, guessValue):
        result = guess_number.guess_int(1,10)
        self.assertEqual(result,7,"should be 7")
    
    @mock.patch('random.randint',return_value=int(363))
    def test_guess_int_10_to_363_should_be_363(self, guessValue):
        result = guess_number.guess_int(10,999)
        self.assertEqual(result,363,"should be 363")
    
    @mock.patch('random.randint',return_value=int(53))
    def test_guess_int_80_to_9_should_be_53(self, guessValue):
        result = guess_number.guess_int(80,9)
        self.assertEqual(result,53,"should be 53")

    @mock.patch('random.randint',return_value=int(23))
    def test_guess_int_23_to_23_should_be_23(self, guessValue):
        result = guess_number.guess_int(23,23)
        self.assertEqual(result,23,"should be 23")

    @mock.patch('random.randint',return_value=int(-300))
    def test_guess_int_minus300_to_minus1_should_be_minus300(self, guessValue):
        result = guess_number.guess_int(-999,-1)
        self.assertEqual(result,-300,"should be -300")

    @mock.patch('random.randint',return_value=int(60))
    def test_guess_int_minus99_to_99_should_be_60(self, guessValue):
        result = guess_number.guess_int(-99,99)
        self.assertEqual(result,60,"should be 60")

    #guess_float
    @mock.patch('random.uniform',return_value=float(3.6))
    def test_guess_float_1_to_10_should_be_3point6(self, guessValue):
        result = guess_number.guess_float(1,10)
        self.assertEqual(result,3.6,"should be 3.6")
    
    @mock.patch('random.randint',return_value=float(0.0))
    def test_guess_float_0_to_0_should_be_0point0(self, guessValue):
        result = guess_number.guess_int(0,0)
        self.assertEqual(result,0.0,"should be 0.0")
        
    @mock.patch('random.uniform',return_value=float(12.4))
    def test_guess_float_11point7_to_12point9_should_be_12point4(self, guessValue):
        result = guess_number.guess_float(11.7,12.9)
        self.assertEqual(result,12.4,"should be 12.4")

    @mock.patch('random.uniform',return_value=float(-23.4466447))
    def test_guess_float_minus_78point91489_to_minus_2point5134_should_be_minus_23point4466447(self, guessValue):
        result = guess_number.guess_float(-78.91489,-2.5134)
        self.assertEqual(result,-23.4466447,"should be -23.4466447")

    @mock.patch('random.uniform',return_value=float(-78.861951))
    def test_guess_float_minus99_to_minus30_should_be_minus_51point861951(self, guessValue):
        result = guess_number.guess_float(-99,-30)
        self.assertEqual(result,-78.861951,"should be -78.861951")

    